import store from "../store";

export const checkAccounts = async () => {
  if (window.ethereum?.isMetaMask) {
    const accounts = await window.ethereum.request({ method: "eth_accounts" });
    if (accounts[0]) {
      store.commit("setAccount", accounts[0]);
    }
    connect();
  }
};

const accountsChangedHandler = (accounts) => {
  store.commit("setAccount", accounts[0] || null);
};

export const disconnect = () => {
  window.ethereum.removeListener("accountsChanged", accountsChangedHandler);
};

export const connect = () => {
  disconnect();
  window.ethereum.on("accountsChanged", accountsChangedHandler);
};

export const loginMetamask = async () => {
  if (window.ethereum.isMetaMask) {
    await window.ethereum.request({ method: "eth_requestAccounts" });
    connect();
  }
};
